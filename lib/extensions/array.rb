class Array
  # Removes payments where recipient and payer are the same person
  def exclude_self_payments
    reject{ |p| p.self_payment? }
  end
end
