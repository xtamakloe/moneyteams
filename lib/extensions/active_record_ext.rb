module Extensions
  module ActiveRecordExt

    extend ActiveSupport::Concern


    # Checks if it's safe to destroy current instance
    def destroyable?(*associations)
      associations.each do |assoc|
        if self.send(assoc.to_s).present?
          self.errors.add(:base, "Cannot remove due to associated "\
                                 "#{assoc.to_s.humanize}.")
          return false
        end
      end
    end



    module ClassMethods
      # Adds Money Gem's composed_of field to instance
      # Params:
      #   +field_name+:: +Symbol+ representing money field name in db
      def money_fields(*field_names)
        field_names.each do |field_name|
          attr_accessible field_name
          composed_of field_name,
            class_name: "Money",
            mapping: [["#{field_name}_in_cents", "cents"], %w(currency currency_as_string)],
            constructor: Proc.new { |cents, currency| Money.new(cents || 0, currency || Money.default_currency) },
            converter: Proc.new { |value| value.respond_to?(:to_money) ? value.to_money : raise(ArgumentError, "Can't convert #{value.class} to Money") }
        end
      end
      alias_method :money_field, :money_fields
    end


  end
end

ActiveRecord::Base.send(:include, Extensions::ActiveRecordExt)

