class Engine

  # Params:
  #   +groups+:: +Array+ of groups or single +group+ to be processed
  def self.process_groups(groups)
    [ groups ].flatten.each do |group|
      process_cycle(group.latest_cycle)
    end
  end



  # Processes cycle for a group
  # Params:
  #   +latest_cycle+:: +Cycle+ to be processed
  def self.process_cycle(latest_cycle)
    return unless latest_cycle

    # Don't process paused cycles
    return unless latest_cycle.paused?

    # Only start new cycle if the current cycle is complete
    # meaning all users have to pay for group to continue
    group                   = latest_cycle.group
    next_cycle_start_date   = latest_cycle.next_cycle_start_date
    penalty_deadline        = latest_cycle.penalty_deadline
    penalty_alert_timespan  = group.penalty_alert_timespan_quantity.
                                send(group.penalty_alert_timespan_unit)

    if latest_cycle.complete?
      # If latest cycle is not the last in the round
      # and today is the date for the next cycle, start next cycle
      if !latest_cycle.last? && today_is?(next_cycle_start_date)
        group.start_new_cycle(latest_cycle)
      end
    else
      # Get unpaid payouts
      unpaid_payouts = latest_cycle.unpaid_payouts

      if unpaid_payouts.present?
        # start date for next cycle
        if today_is?(next_cycle_start_date)
          # pause cycle
          latest_cycle.pause!

          # send alert that cycle group cannot
          # continue until all arrears are paid
          # so new cycle won't be created

        # x days to penalty deadline
        elsif today_is?(penalty_deadline, penalty_alert_timespan)
          # send deadline warning alerts
         latest_cycle.delay.
           send_penalty_alerts(:penalty_warning, unpaid_payouts)

        # on penalty deadline
        elsif today_is?(penalty_deadline)
          # add penalty to pay_outs
          unpaid_payouts.each { |p| p.apply_penalty!; p.reload }

          # send alerts to members
          latest_cycle.delay.
            send_penalty_alerts(:penalty_incurred, unpaid_payouts)
        end
      end
    end
  end



  # Determines how many days present day is from
  # date provided
  # Params:
  #   +date+:: future +date+
  #   +timespan+:: number of days to +date+ provided
  def self.today_is?(date, timespan=0.days)
    return true if Date.today == (date - timespan)
  end




  # Sends alert to members based on alert settings of group
  # Params:
  #   +member+:: +Member+ to send alert to
  #   +group+:: +Group+ member belongs to
  #   +bundle+:: +Hash+ containing details of alert
  def self.send_alert(member, group, bundle)
     case group.alert_mode
     when 1
       send_email(member, bundle[:event], bundle)
     when 2
       send_sms(member, bundle[:event], bundle)
     end
  end



  # Sends an email to a member
  # Params:
  #   +member+:: +member+ to send email to
  #   +event+:: +Symbol+ for event that triggered alert
  #   +bundle+:: +Hash+ containing details of alert
  def self.send_email(member, event, bundle)
    case event
    when :cycle_start
      GroupMailer.delay.cycle_start_alert(member, bundle)
    when :penalty_warning
      GroupMailer.delay.penalty_warning_alert(member, bundle)
    when :penalty_incurred
      GroupMailer.delay.penalty_incurred_alert(member, bundle)
    end
  end



  # Generates sms alert and sends it to a group member
  # Params:
  #   +member+:: +member+ to send sms to
  #   +event+:: +Symbol+ for event that triggered alert
  #   +bundle+:: +Hash+ containing details of alert
  def self.send_sms(member, event, bundle)
    msg =   "Hi #{member.first_name}, "
    msg <<  case event
            when :cycle_start
              ending  = case bundle[:payment].type
                        when "PayIn"
                          "You're gonna get paid #{ bundle[:pay_in] }."
                        when "PayOut"
                          "You have to pay #{ bundle[:pay_out] }."
                        end
              "it's the start of a new cycle. #{ending}"
            when :penalty_warning
              "You are going to be penalised #{@bundle[:penalty_amount]} soon."\
              " You will have to pay #{@bundle[:amount]} if you don't pay "\
              "by #{@bundle[:deadline]}."
            when :penalty_incurred
              "You have been penalised #{ bundle[:amount].to_money } for "\
              "not paying on time."
            end

    Sms.delay.send_message(member.msisdn, msg)
  end

end
