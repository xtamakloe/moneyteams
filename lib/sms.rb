module Sms

  # Send message via SMS
  # use account name as source unless
  # specified in params[:source]
  def self.send_message(to_num, sms_message, account=nil)
    phone_number_arrays = []
    to_number           = [to_num].flatten

    # break down array into an array containing
    # arrays of phone numbers [[1 2 3 ], [1 2 3 ]]
    slice_size  = case to_number.size
                  # when 0..50   then 20
                  when 0..20   then 5
                  when 51..200 then 50
                  else 100
                  end

    to_number.each_slice(slice_size).to_a.each do |the_slice|
      phone_number_arrays << the_slice
    end

    source = account.code if account

    sendsms_results = send_sms(phone_number_arrays, sms_message, source)

    # Log no. of sms sent successfully

    return sendsms_results[:status]
  end



  # Determine number of messages based on number of recipients
  # and the length of each message. 1 sms = 160 chars
  def self.get_message_count(msg_length, no_of_recipients)
    res       = msg_length.divmod(160)
    q         = res.first
    r         = res.last
    msg_parts = (r == 0) ? q : q + 1

    msg_parts * no_of_recipients
  end


  # Sends SMS via RouteSMS
  # Params:
  #   +phone_arrays+:: an +array+ containing arrays of phone
  #                     numbers [[1 2 3 ], [1 2 3 ]]
  # Returns
  #   +Hash+ containing operation success status and
  #   successful recipients or error messages if any
  #   {success: .., successful_recipients: ..., errors: ...}
  def self.send_sms(phone_arrays, message, source=nil)
    url      = 'http://smsplus1.routesms.com:8080/bulksms/bulksms'
    username = ''
    password = ''
    source   = source || 'mymoneyteam'
    type     = '5'
    dlr      = '1'

    request_array         = []
    response_array        = []
    errors                = []
    successful_recipients = []

    #-----------------------------------------------------------------------"

    #DEBUG puts "------------------------------------------"
    #DEBUG puts "Debugging Info"

    #DEBUG phone_arrays.each do |phone_arr|
    #DEBUG   puts "SMS To: " + phone_arr.join(',').to_s
    #DEBUG end

    #DEBUG puts  "---------------------------------------"
    #DEBUG puts  "Message: " + message if message
    #DEBUG puts  "---------------------------------------"
    #DEBUG puts  "Message Length: " + message.length.to_s if message
    #DEBUG puts  "---------------------------------------"
    # DEBUG puts  "No. of SMS Sent: "\
    #  "#{ get_message_count(message.length,
    #      phone_arrays.flatten.length).to_s }"

    phone_arrays.each do |phone_array|
      request = Typhoeus::Request.new(
        url,
        method: :get,
        params: { username:    username,
                  password:    password,
                  type:        type,
                  dlr:         dlr,
                  destination: phone_array.join(','),
                  source:      source,
                  message:     message })
      #DEBUG puts  "---------------------------------------"
      #DEBUG puts  "URL: " + request.url
      #DEBUG puts  "---------------------------------------"

      request.on_complete do |response|
        response_array << response
      end

      request_array << request
    end
    #DEBUG puts "Time:#{Time.now.ctime}"
    #DEBUG puts "------------------------------------------"

    # # For local development
    return { status: true ,
             successful_recipients: phone_arrays.flatten,
             errors: errors }
    # #----------------------------------------------------'

    # Perform HTTP requests
    hydra = Typhoeus::Hydra.new
    request_array.each { |request| hydra.queue(request) }
    hydra.run

    # Examine response of HTTP requests
    response_array.each do |response|
      #DEBUG puts "---------------------------------------"
      #DEBUG puts "Response Code: " + response.code.to_s
      #DEBUG puts "---------------------------------------"
      #DEBUG puts "Response Body: " + response.body
      #DEBUG puts "---------------------------------------"
      #DEBUG puts "Response Time: " + response.time.to_s
      #DEBUG puts "---------------------------------------"
      #DEBUG puts "Response Headers: " + response.headers.to_s
      #DEBUG puts "---------------------------------------"

      # Debugging Info
      if response.code == 408
        #DEBUG puts "----------------------------------------------------"
        #DEBUG puts "Response Code: " + response.code.to_s
        #DEBUG puts "Timed Out"
        #DEBUG puts "----------------------------------------------------"
        errors << "Timed Out"
      elsif response.code == 0
        #DEBUG puts "----------------------------------------------------"
        #DEBUG puts "Response Code: " + response.code.to_s
        #DEBUG puts "Could not get an http response, something's wrong."
        #DEBUG puts "----------------------------------------------------"
        errors << "Could not get an http response, something's wrong."
      end

      # Successful Response
      if response.code == 200
        response.body.split(',').each do |response_arr| # ["1701|+233244817759:00c1b30e-b3bd-4ae9-9404-4a9a8cfbb40b",...]
          code     = response_arr.split('|')[0]         # ["1701", "+233244817759:00c1b30e-b3bd-4ae9-9404-4a9a8cfbb40b,"]...
          # phone_no = response_arr.split('|')[0]         # June 2012: new return ["1701|233244817759|dc683e4f-1f8d-492c-abb8-a7828a346f19"]

          # Message submitted successfully
          if code.to_s == "1701"
            successful_recipients << response_arr
          else
            errors << response_arr  # TODO: loop thru errors and determine
                                    #       specific errors based on 'code'.
                                    #       eg. 1706 Invalid destination
          end
        end
      end
    end

    # Process errors
    status = errors.present? ? false : true

    { :status: status ,
      :successful_recipients: successful_recipients,
      :errors: errors }
  end

end
#  Response Body: 1701|+233207711598:ee620926-6083-4ffe-88d6-3cbc65b10f92,
              #   1701|+233244817759:00c1b30e-b3bd-4ae9-9404-4a9a8cfbb40b,
              #   1701|+233260700593:21523eb4-8faf-4edd-93c7-166916a2642c
# new return 1701|233244817759|58fca81b-8335-4625-a7d7-af48cc57927b
#
#
# 1701 = Success
# 1702 = Invalid URl Error
# 1703 = Invalid value in username or password field
# 1704 = Invalid value in type field
# 1705 = Invalid Message
# 1706 = Invalid Destination
# 1707 = Invalid Source (Sender)
# 1708 = Invalid value for DLR field
# 1709 = User validation failed
# 1710 = Internal Error
# 1025 = Insufficient Credit#

