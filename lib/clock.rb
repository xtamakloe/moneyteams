ENV['RAILS_ENV'] = ARGV.first || ENV['RAILS_ENV'] || 'development'
require File.expand_path(File.dirname(__FILE__) + "/../config/environment")
require File.expand_path(File.dirname(__FILE__) + "/../config/boot")

require 'clockwork'

include Clockwork

every(12.hours, 'Process Payments') { Engine.delay.process_groups(Group.all) }
