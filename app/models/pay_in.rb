=begin
# Payments received by a member from other group members
class PayIn < Payment

  after_create :mark_as_paid

  def mark_as_paid
    self.paid!
  end
end
=end
