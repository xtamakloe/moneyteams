class Member < ActiveRecord::Base
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  attr_accessible :email, :password, :password_confirmation, :remember_me,
                  :first_name, :last_name, :username, :avatar


  has_one   :member_spec, dependent: :destroy
  has_many  :memberships, dependent: :destroy
  has_many  :groups, through: :memberships
  has_many  :payments
  has_many  :invites #, dependent: :destroy

  has_attached_file :avatar,
                    styles: {
                      thumb: '50x50>',
                      large: '128x128>' },
                    default_url: '/assets/avatars/missing_:style.png'


  before_destroy { destroyable?(:payments)} # prevent removal if payments present
  after_create :find_invitations


  # Overrides Member#payments to return all payments
  # involving this member INCLUDING those PAID + RECIEVED
  # by themselves
  # Apply self_payment? to reject
  def payments
    Payment.where("member_id = ? OR recipient_id = ?", self.id, self.id)
  end


  # Returns payments made/to be made by this member
  # EXCLUDING those TO themselves
  def paying_payments
    # self.payments.where("recipient_id != ?", self.id)
    Payment.where("member_id = ?", self.id).where("recipient_id != ?", self.id)
  end


  # Returns payments received/to be received by this member
  # EXCLUDING those FROM themselves
  def receiving_payments
    Payment.where("recipient_id = ?", self.id).where("member_id != ?", self.id)
  end


  # Returns unpaid payments involving this member that are coming
  # up soon based on the active cycles of the teams he belongs
  # i.e. payments to be received
  #      payments to pay
  def upcoming_payments
    self.
      # get payments for active cycles of teams logged
      # in member is involved in
      active_cycles.map(&:payments).flatten.
      # get payments belonging to logged in member
      select{|p| [ p.member_id, p.recipient_id ].include?(self.id) }.
      # get unpaid payments
      reject{ |p| p.paid? }.
      # remove self payments
      exclude_self_payments
  end


  # Filters payments to EXCLUDE payments PAID TO or RECEIVED FROM themselves
  # and returns this as an +array+
  def payments_excluding_self
    payments.excluding_self_payments
  end


  def payments_receiving_this_month
    receiving_payments.made_in_month_of(Date.today.month, Date.today.year)
  end


  def payments_paying_this_month
    paying_payments.made_in_month_of(Date.today.month, Date.today.year)
  end


  def is_group_admin?(group)
    membership = self.is_group_member?(group)
    return membership.admin? if membership

    false
  end


  def is_group_member?(group)
    self.memberships.find_by_group_id(group.id) if group
  end


  def full_name
    if self.first_name.present? || self.last_name.present?
      "#{self.first_name} #{self.last_name}".strip
    else
      self.email
    end
  end


  def payment_for(cycle)
    #--
    # TODO: payments here refers to only payments made, we
    #       need to account for payments recieved
    #++
    self.payments.find_by_cycle_id(cycle.id)
  end


  def active_cycles
    self.groups.map(&:latest_cycle).select{|c| !c.complete? if c}
  end


  private


  def find_invitations
    self.invites << Invite.where(email: self.email)
  end

end
