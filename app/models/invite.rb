class Invite < ActiveRecord::Base
  attr_accessible :email, :first_name, :last_name, :join_link


  belongs_to :group
  belongs_to :member


  # after_create :send_invite_alert


  validates :email, uniqueness: { scope: :group_id,
                                  message: "An invite has already been "\
                                           "sent to this email address" }


  def send_invite_alert
    # send email or sms invite
    # containing link to join page
    GroupMailer.delay.group_invite_alert(self)
  end



  def full_name
    "#{self.first_name} #{self.last_name}".strip
  end
end
