class Payment < ActiveRecord::Base
  include PublicActivity::Common

  attr_accessible :member_id,
                  :recipient_id,
                  :cycle_id,
                  :amount,
                  :type,
                  :paid_at
  money_fields    :cycle_amount,
                  :penalty_amount


  belongs_to :member
  belongs_to :recipient, class_name: 'Member'
  belongs_to :cycle


  scope :paid, -> { where('paid_at IS NOT null') }
  scope :unpaid, -> { where(paid_at: nil) }
  scope :excluding_self_payments, -> { all.exclude_self_payments }

  scope :made_in_month_of, lambda { |month,year|
    d = Date.parse("1/#{month}/#{year}")
    where('created_at BETWEEN ? AND ?', d.beginning_of_month, d.end_of_month) }
  scope :made_in_year, lambda { |year|
    d = Date.parse("1/1/#{year}")
    where('created_at BETWEEN ? AND ?', d.beginning_of_year, d.end_of_year) }


  def amount
    cycle_amount + penalty_amount
  end


  def paid?
    self.paid_at?
  end


  def paid!(timestamp=DateTime.now)
    self.update_attributes(paid_at: timestamp)
  end


  def self_payment?
    self.member_id == self.recipient_id
  end


  # Increases amount to be paid due to penalty infraction
  def apply_penalty!
    # self.update_attributes(amount: self.group.penalty_amount + self.amount)
    self.update_attributes(penalty_amount: self.cycle.penalty_amount)
    create_activity action: 'apply_penalty',
                    owner: self.cycle.group,
                    params: { penalty_amount: self.penalty_amount.to_s }
  end


  def penalized?
    !(self.penalty_amount == 0)
  end


end
