=begin
# Payments made by a member to other group members
class PayOut < Payment

  def apply_penalty!
    self.update_attributes(amount: self.group.penalty_amount + self.amount)
  end
end
=end
