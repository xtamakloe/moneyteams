class Membership < ActiveRecord::Base
  include PublicActivity::Common

  attr_accessible :group_id, :member_id, :admin, :group, :member, :cycle_position

  belongs_to :group
  belongs_to :member

  after_create :set_position

  # #--
  # TODO: this validation is preventing setting the individual positions
  #       and then saving at once from working
  #++
  # validates :cycle_position, uniqueness: { scope: :group_id }

  def set_position
    self.update_attributes(cycle_position: self.group.memberships.count)
  end
end
