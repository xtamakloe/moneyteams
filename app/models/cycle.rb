class Cycle < ActiveRecord::Base
  include PublicActivity::Common

  attr_accessible :group_id,
                  :recipient_id,
                  :member_count,
                  :number,
                  :current,
                  :complete,
                  :emails_sent,
                  # :recipients_amount,
                  # :payers_amount,
                  # :penalty_amount,
                  :round,
                  :last,
                  :paused
  money_fields :recipients_amount, :payers_amount, :penalty_amount


  belongs_to  :group
  has_many    :payments # only member deletion can delete payment
  # has_many    :pay_ins
  # has_many    :pay_outs
  belongs_to  :recipient, class_name: 'Member'

  # validates :current, :uniqueness # can only be one current cycle

  after_create :setup_cycle


  def next_cycle_start_date_pretty
    if next_cycle_start_date > Date.today # if it's in the future
      next_cycle_start_date
    end
  end


  def next_cycle_start_date
    start_date + self.group.payment_timespan
  end



  def penalty_deadline
    start_date + penalty_timespan
  end



  def start_date
    self.created_at.to_date
  end



  def end_date
    self.next_cycle_start_date - 1.day
  end



  # def payment_timespan
  #   self.group.payment_cycle_quantity.send(self.group.payment_cycle_unit)
  # end
  # def payment_timespan
  #   self.group.payment_timespan
  # end



  def penalty_timespan
    self.group.penalty_cycle_quantity.send(self.group.penalty_cycle_unit)
  end



  def archive!
    self.update_attributes(current: false)
  end



  def set_round!(rnd)
    self.update_attributes(round: rnd)
  end



  def pause!
    self.update_attributes(paused: true)
  end



  def unpause!
    self.update_attributes(paused: false)
  end



  def unpaid_payouts
    self.pay_outs.unpaid
  end



  def send_cycle_start_alerts
    # send alert for upcoming cycle
    #   email, amount to pay, who to pay to

    pay_out_amt = self.group.cycle_amount
    payments    = self.payments
    pay_in_amt  = pay_out_amt * payments.count

    bundle = { event: :cycle_start,
               pay_in: pay_in_amt,
               pay_out: pay_out_amt }

    payments.each do |p|
      bundle[:payment]  = p
      Engine.send_alert(p.member, self.group, bundle)
    end
  end



  def send_penalty_alerts(type, payment_array)
    pymnts      = [ payment_array ].flatten
    amount      = pymnts.first.try(:amount)

    bundle      = { event: type,
                    deadline: self.penalty_deadline,
                    amount: amount.to_money,
                    penalty_amount: self.group.penalty_amount }

    pymnts.each do |p|
      Engine.send_alert(p.member, self.group, bundle)
    end
  end



  def acknowledge_payment(payment, timestamp=DateTime.now)
    if self.payments.include?(payment) && !payment.paid?
      payment.paid!(timestamp)
      # self.reload
      self.update_attributes(complete: true) if self.payments_complete?

      self.group.stop_round! if self.last? # stop round if this is the last cycle

      payment.create_activity action: 'register', owner: self.group

      true
    end
  end



  def payments_complete?
    !self.payments.map(&:paid?).include?(false)
  end



  def payments_made
    self.payments.paid
  end



  def percentage_paid
    self.payments_made.sum(&:cycle_amount).to_f / self.payments.sum(&:cycle_amount).to_f * 100
  end



  def receiving_member
    # method 1: uses more resources
    # self.group.memberships.find_by_cycle_position(self.number).member
    # method 2: less resources but relies on pay_in being created
    # self.pay_ins.first.member
    self.recipient
  end



  private


  def setup_cycle
    set_member_info
    create_payments
  end



  def set_member_info
    memberships = self.group.memberships
    recipient   = memberships.where(cycle_position: self.number).first.try(:member)

    self.update_attributes(member_count: memberships.count,
                           recipient_id: recipient.try(:id))
  end



  def create_payments
    self.group.memberships.each do |m|
      self.
        payments.
        create(member_id:    m.member_id,
               recipient_id: self.recipient_id,
               cycle_amount: self.payers_amount,
               paid_at:      m.member_id == self.recipient_id ? DateTime.now : nil)
    end
  end
end



=begin
  def create_payments
    self.group.memberships.each do |membership|
      attrs = { member_id: membership.member_id, amount: self.payers_amount }

      if membership.cycle_position == self.number # recieving member
        self.pay_ins.create(attrs)
      else
        self.pay_outs.create(attrs)
      end
    end
  end
=end
