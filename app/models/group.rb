class Group < ActiveRecord::Base
  include PublicActivity::Common

  CYCLE_UNITS = %w( days weeks months )

  attr_accessible :name,
                  :description,
                  # :cycle_amount, # amount paid by each member per cycle
                  # :penalty_amount,
                  :start_date,  # to start first cycle automatically
                  :alert_mode,
                  :payment_cycle_quantity,
                  :payment_cycle_unit,
                  :penalty_cycle_quantity,
                  :penalty_cycle_unit,
                  :penalty_alert_timespan_quantity,
                  :penalty_alert_timespan_unit,
                  :in_session,
                  :round
  money_fields :penalty_amount, :cycle_amount


  has_many :memberships, dependent: :destroy
  has_many :members, through: :memberships
  has_many :cycles,  dependent: :destroy
  has_many :invites, dependent: :destroy


  validates :name, presence: true
  validates :payment_cycle_unit, presence: true,
                                 inclusion: {
                                    in: CYCLE_UNITS,
                                    message: "%{value} is not a "\
                                             "valid unit. "\
                                             "Please choose from "\
                                             "DAYS, WEEKS or MONTHS"}
  scope :active, where(in_session: true)
  scope :inactive, where(in_session: false)


  def self.pretty_cycle_units
    CYCLE_UNITS.collect { |u| [u.gsub("s", "(s)"), u] }
  end



  def has_members?
    self.members.present?
  end



  def invited?(email)
    self.get_invite(email).present?
  end



  def get_invite(email)
    self.invites.find_by_email(email)
  end



  def admin
    self.memberships.find_by_admin(true).try(:member)
  end



  # Returns +array+ of +cycles+ in latest/current round
  def latest_round_cycles
    # self.cycles.where(round: latest_cycle.try(:round)) if latest_cycle
    self.cycles.where(round: self.round)
  end



  def latest_cycle # current_cycle
    # self.cycles.where(current: true).first
    latest_round_cycles.where(current: true).first
  end



  # Returns +array+ of +members+ paid in latest/current round
  def members_paid_in_latest_round
    self.latest_round_cycles.try(:map, &:receiving_member) || []
  end



  def members_not_paid_in_latest_round
    self.memberships.map(&:member) - members_paid_in_latest_round
  end



  def reverse_member_positions!
    g_memberships = self.memberships.order(:cycle_position)
    count = g_memberships.count

    g_memberships.each_with_index do |membership, index|
      membership.update_attribute(:cycle_position, count - index)
    end
  end



  def payment_timespan
    self.payment_cycle_quantity.send(self.payment_cycle_unit)
  end



  # Returns time period for the position specified during this cycle
  # Can only be used when group is in session
  def period_for_member_at_position(position)
    # if there's been no disruption, use start date
    # else use last cycle position + end date to calculate future dates? recheck logic
    # position 5 is gonna get paid in 5th cycle
    # so all i need to do is calculate start_date of 5th cycle and end date of 5th cycle

    # timespan to position period beginning
    # first cycle start date
    start_date = latest_round_cycles.where(number: 1).first.try(:start_date)

    if start_date
      (position - 1).times { start_date += payment_timespan }
    end

    return { start: start_date, end: (start_date + payment_timespan) - 1.day }
  end



  def start_round!
    self.update_attributes(in_session: true)
  end



  def stop_round!
    #--
    # TODO: when a round is stopped, all cycles should be
    # stopped too. else new cycle can't be started
    # 2 ways to stop cycle:
    #   manually eg. members decide to stop
    #   automatically eg. for now only when last payment is made
    #++
    if self.update_attributes(in_session: false)
      create_activity action: 'stop', owner: self
    end
  end



  def set_round!(previous_cycle)
    self.update_attributes(round: previous_cycle.round + 1)
  end



  def start_new_cycle(previous_cycle=nil)
    # don't proceed if the latest cycle hasn't been completed
    return false if self.latest_cycle && !self.latest_cycle.complete?

    payers_amount = self.cycle_amount
    recipients_amount  = payers_amount * self.members.count

    # if there's a previous cycle which is not the last in the round
    if previous_cycle && !previous_cycle.last?
      # get no of cycles in that round and increase count
      cycle_number = self.cycles.where(round: self.round).count + 1
      # check if this new cycle should be the last
      is_last      = true if cycle_number == self.memberships.count
    end

    new_cycle = self.cycles.create(recipients_amount: recipients_amount,
                                   payers_amount: payers_amount,
                                   penalty_amount: self.penalty_amount,
                                   current: true,
                                   number: cycle_number || 1,
                                   last: is_last || false)
    if new_cycle
      # set previous as not current
      previous_cycle.archive! if previous_cycle

      # if group isn't running
      unless self.in_session?
        # set group as running
        self.start_round!
        # make sure rignt round number is set
        # right now round is set only when previous cycle was a last
        # cycle. need to account for case where there is a restart after
        # previous cycle was stopped; do we create new round? or
        # continue with old round
        self.set_round!(previous_cycle) if previous_cycle && previous_cycle.last?
      end

      # set cycle round to group round
      new_cycle.set_round!(self.round)

      # send alerts
      new_cycle.delay.send_cycle_start_alerts


      # create newsfeed item
      new_cycle.create_activity action: 'create', owner: self

      return new_cycle
    end

    false
  end



  # Creates a new invite
  # Params:
  #   +invitee+:: +Hash+ containing recipient's details
  # Returns:
  def create_invite_for(invitee)
    email     = invitee[:email]
    firstname = invitee[:first_name]
    last_name = invitee[:last_name]
    invite    = self.invites.new(invitee)

    existing_user = Member.find_by_email(email)
    if existing_user
      unless existing_user.is_group_member?(self)
        invite.member = existing_user
      else
        error = "Attempting to invite an existing team member"
      end
    end

    if error.nil?
      error = invite.errors.full_messages unless invite.save
    end

    return { status: error.nil?, error: error }
  end



  def add_member(member)
    unless member.is_group_member?(self) # not already a member
      unless self.in_session? # session not running
        if self.has_members?  # check for invitation if group
                              # already has members
          invite = self.get_invite(member.email)
          if invite
            create_membership(member)
            invite.destroy
          else
            error = "Member not invited"
          end
        else
          create_membership(member)
        end
      else
        error = "Cannot add members once a round has started!"
      end
    end

    return { status: error.nil?, error: error }
  end



  def create_membership(member)
    membership = self.memberships.create(member: member, admin: !self.has_members?)
    membership.create_activity :create, owner: self, recipient: member
  end



  def remove_member(member)
    if member.is_group_member?(self)
      unless member.is_group_admin?(self) # not group admin
        unless self.in_session? # session not running
          membership = self.memberships.find_by_member_id(member.id)
          membership.create_activity :destroy, owner: self, recipient: member
          error = "Error removing membership" unless membership.destroy
        else
          error = "A round is in progress"
        end
      else
        error = "Admin cannot leave team."
      end
    else
      error = "You don't belong to this team."
    end

    return { status: error.nil?, error: error }
  end

end
