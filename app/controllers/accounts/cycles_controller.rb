class Accounts::CyclesController < Accounts::BaseController
  def index
    @cycles = current_member.active_cycles
  end
end
