class Accounts::BaseController < ApplicationController
  before_filter :authenticate_member!
  before_filter :load_layout

  def load_layout
    @layout = 'accounts'
  end
end
