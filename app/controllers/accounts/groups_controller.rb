class Accounts::GroupsController < Accounts::BaseController


  def index
    @groups = current_member.groups
  end



  def new
    @group = Group.new
  end



  def create
    @group = Group.new(params[:group])

    if @group.save && @group.add_member(current_member)
      flash[:success] = "You've started a new team!"
      redirect_to group_url(@group)
    else
      flash[:error] = @group.errors.full_messages
      render :action => "new"
    end
  end


end
