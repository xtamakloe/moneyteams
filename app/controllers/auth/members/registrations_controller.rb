class Auth::Members::RegistrationsController < Devise::RegistrationsController

  layout 'app'


  # NB: overrode this method because after_update_path_for wasn't working
  #     otherwise
  def update
    if resource.update_with_password(params[resource_name])
      set_flash_message :success, :updated
      # Line below required if using Devise >= 1.2.0
      sign_in resource_name, resource, bypass: true
      redirect_to after_update_path_for(resource)
    else
      clean_up_passwords(resource)
      redirect_to after_update_path_for(resource), flash: { alert: "Error message" }
    end
  end


  protected


  def after_update_path_for(resource)
    edit_hub_account_url(resource)
  end


  def after_sign_up_path_for(resource)
    if session[:return_to]
      destination_url     = session[:return_to]
      session[:return_to] = nil
      destination_url
    else
      hub_root_url
    end
  end

end
