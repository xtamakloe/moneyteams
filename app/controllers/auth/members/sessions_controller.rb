class Auth::Members::SessionsController < Devise::SessionsController

  layout 'app'


  def new
    render 'auth/members/registrations/new'
  end


  protected

  def after_sign_in_path_for(resource)
    if session[:return_to]
      destination_url     = session[:return_to]
      session[:return_to] = nil
      destination_url
    else
      hub_root_url
    end
  end



  def after_sign_out_path_for(resource)
    welcome_url
  end

end
