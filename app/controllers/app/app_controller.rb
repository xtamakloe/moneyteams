class App::AppController < ApplicationController
  protect_from_forgery

  before_filter :authenticate_member!

  layout 'app'
end
