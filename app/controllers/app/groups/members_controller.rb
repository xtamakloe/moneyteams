class App::Groups::MembersController < App::Groups::BaseController


  def show
    @member = @group.members.find(params[:id])
  end


  def index; end

end
