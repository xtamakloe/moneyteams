class App::Groups::GroupsController < App::Groups::BaseController
  # include PublicActivity::StoreController

  # view all public groups, have to be logged in tho
  # groups will have to be marked public and private
  def index
    @groups = Group.all
  end


  def show
    @latest_cycle       = @group.latest_cycle
    @group_invites      = @group.invites
    @group_cycles       = @group.cycles
    @payment_cycle_unit = @group.payment_cycle_unit
    @cycle_status       = @group.in_session? && @latest_cycle.present?
    @activities         = PublicActivity::Activity.order("created_at desc").
                            includes(:trackable).
                            where(owner_id: @group.id)
    @active             = params[:active]
  end


  def edit
    @admin       = current_member.is_group_admin?(@group)
    @not_running = !@group.in_session?
  end


  def update
    if @group.update_attributes(params[:group])
      flash[:success] = "Rules successfully updated!"
      redirect_to group_url(@group)
    else
      flash[:error] = "Error changing group settings!"
      render action: "edit "
    end
  end


  # GET members/join"
  def add_member
    op = @group.add_member(current_member)

    if op[:status] == true
      flash[:success] = "Congratulations! You are now a member of #{ @group.name }."
    else
      flash[:error] = "Could not add member! #{op[:error]}"
    end

    redirect_to group_url(@group)
  end


  # PUT leave
  def remove_member
    op = @group.remove_member(current_member)

    if op[:status] == true
      flash[:success] = "You have successfully left #{ @group.name }!"
      redirect_to hub_root_url(@group) and return
    else
      flash[:error] = "Could not leave team! #{op[:error]}"
      redirect_to group_url(@group)
    end
  end


  # Starts a new round of cycles
  # GET rounds/start
  def start_round
    if @group.start_new_cycle(@group.latest_cycle)
      flash[:notice] = "Round #{ @group.round } started!"
      # redirect_to current_cycle_group_url(@group)
      redirect_to group_url(@group)
    else
      flash[:error] = "New round couldn't be started!"
      redirect_to group_url(@group)
    end
  end



  # GET rounds/pause
  def pause_round
    flash[:success] = "Pause nigguh"
    redirect_to group_url(@group)
  end



  # GET rounds/stop
  def stop_round
    flash[:success] = "Stop nigguh"
    redirect_to group_url(@group)
  end


  # GET current
  def current_cycle
    @cycle = @group.latest_cycle
    redirect_to group_url unless @cycle
  end
end
