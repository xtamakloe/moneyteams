class App::Groups::CyclesController < App::Groups::BaseController


  def index
    @rounds = @group.cycles.group_by(&:round)
  end


  def show
    @cycle = @group.cycles.find(params[:id])
  end


  # PUT
  def acknowledge_payment
    @cycle   = @group.cycles.find(params[:id])
    @payment = @cycle.payments.find(params[:p_id])

    if @cycle.acknowledge_payment(@payment)
      # @payment.create_activity action: 'register', owner: @group
      flash[:success] = "Payment information saved successfully"
    else
      flash[:error] = "Payment could not be saved"
    end

    redirect_to group_url(@group)
  end
end
