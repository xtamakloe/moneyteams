class App::Groups::InvitesController < App::Groups::BaseController


  def create
    op = @group.create_invite_for(params[:invite])

    if op[:status] == true
      flash[:success] = "Invite sent!"
    else
      flash[:error] = "Could not send invite! #{op[:error]}"
    end

    redirect_to group_url(@group, active: 'members')
  end


  def destroy
    @invite = @group.invites.find(params[:id])
    @invite.destroy

    flash[:success] = "Invite revoked"
    redirect_to group_url(@group, active: 'members')
  end

end
