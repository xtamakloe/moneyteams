class App::Groups::BaseController < App::AppController

  before_filter :load_group


  protected


  def load_group
    find_group
    verify_group_access!
  end


  def find_group
    @group = Group.find_by_id(params[:group_id] || params[:id])

    unless @group
      flash[:alert] = "Group doesn't exist"
      redirect_to member_signed_in? ? hub_root_url : root_url and return
    end
  end


  # Checks permissions of member trying to access page
  # Adds member if team has no members yet
  def verify_group_access!
    if @group.has_members?
      unless current_member.is_group_member?(@group) ||
               @group.invited?(current_member.email)
        redirect_to hub_root_url, alert: "You do not have access to #{ @group.name }!"
      end
    else
      flash.clear
      flash[:success] = "Welcome to Team #{ @group.name }!"
      @group.add_member(current_member)
    end

  end

end
