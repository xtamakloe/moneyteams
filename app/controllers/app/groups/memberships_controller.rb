class App::Groups::MembershipsController < App::Groups::BaseController


  def update_multiple
    values = params[:memberships].values.collect{|m| m['cycle_position']}
    if values.uniq.length == values.length
      params[:memberships].each do |value|
        new_position = value[1][:cycle_position]
        membership   = @group.memberships.find(value[0])
        if membership
          current_position = membership.cycle_position
          unless current_position.to_s == new_position.to_s
            if membership.update_attributes(cycle_position: new_position)
              membership.create_activity action: 'change_position',
                                         params: { previous_position: current_position },
                                         owner: @group,
                                         recipient: membership.member
            end
          end
        end
      end
      flash[:success] = "Positions updated successfully"
    else
      flash[:error] = "Only one member can occupy a specific position at one time"
    end
    redirect_to group_url(@group)
  end
end
