class App::Hub::DashboardController < App::Hub::BaseController


  def show
    cm                  = current_member
    @active_cycles      = cm.active_cycles
    @members_payments   = cm.upcoming_payments
    @receiving_in_month = cm.payments_receiving_this_month
    @paying_in_month    = cm.payments_paying_this_month
  end

end
