class App::Hub::PaymentsController < App::Hub::BaseController


  def index
    @title, @month, @year = "", params[:month], params[:year] || Date.today.year

    #--
    # TODO: implement filter by team
    #++

    payments = case params[:payment_type]
               when 'paid'
                 @title << "Payments you made  "
                 current_member.paying_payments.paid
               when 'received'
                 @title << "Payments you received "
                 current_member.receiving_payments.paid
               else
                 @title << "Payments "
                 current_member.payments.paid
               end

    @payments  = if @month.present?
                  @title << "in #{Date.parse("1/#{@month}/#{@year}").strftime("%B %Y")}"
                  payments.made_in_month_of(@month, @year)

                elsif @year.present?
                  @title << "in #{ @year }"
                  payments.made_in_year(@year)

                else
                  @title << "Ever"
                  payments
                end

    @payments = @payments.paid.excluding_self_payments

    respond_to do |format|
      format.html { render :index }
      format.js
    end
  end

end
