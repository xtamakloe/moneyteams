class App::Hub::CyclesController < App::Hub::BaseController


  def index
    @cycles = current_member.active_cycles
  end

end
