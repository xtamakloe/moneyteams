class Site::PagesController < ApplicationController

  layout 'app'


  def coming_soon
    EmailSignup.create(email: params[:email]) if request.post?

    render layout: nil
  end


  def home
    @group = Group.new
  end


  def login_or_register
    @group = Group.find(params[:id])
  end

end
