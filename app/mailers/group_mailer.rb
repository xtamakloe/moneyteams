class GroupMailer < ActionMailer::Base
  default from: "from@example.com"



  def cycle_start_alert(member, bundle)
    @member = member
    @bundle = bundle

    mail(:to => "#{member.full_name} <#{member.email}>",
         :subject => "MyMoneyTeam - Penalty Incurred")
  end



  def penalty_warning_alert(member, bundle)
    @member = member
    @bundle = bundle

    mail(:to => "#{member.full_name} <#{member.email}>",
         :subject => "MyMoneyTeam - Upcoming Penalty")
  end



  def penalty_incurred_alert(member, bundle)
    @member = member
    @bundle = bundle

    mail(:to => "#{member.full_name} <#{member.email}>",
         :subject => "MyMoneyTeam - Penalty Incurred")
  end



  def group_invite_alert(invite)
    @invite   = invite
    @group    = invite.group
    @members  = @group.members

    mail(:to => "#{invite.full_name} <#{invite.email}>",
         :subject => "MyMoneyTeam Invitation")
  end



  # Generating urls
  # <%= url_for(:host => "example.com",
  #             :controller => "welcome",
  #             :action => "greeting") %>
  # <%= user_url(@user, :host => "example.com") %>
end
