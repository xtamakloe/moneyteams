# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130703190524) do

  create_table "activities", :force => true do |t|
    t.integer  "trackable_id"
    t.string   "trackable_type"
    t.integer  "owner_id"
    t.string   "owner_type"
    t.string   "key"
    t.text     "parameters"
    t.integer  "recipient_id"
    t.string   "recipient_type"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  add_index "activities", ["owner_id", "owner_type"], :name => "index_activities_on_owner_id_and_owner_type"
  add_index "activities", ["recipient_id", "recipient_type"], :name => "index_activities_on_recipient_id_and_recipient_type"
  add_index "activities", ["trackable_id", "trackable_type"], :name => "index_activities_on_trackable_id_and_trackable_type"

  create_table "cycles", :force => true do |t|
    t.integer  "group_id"
    t.boolean  "current",                    :default => false
    t.boolean  "complete",                   :default => false
    t.integer  "recipients_amount_in_cents"
    t.integer  "payers_amount_in_cents"
    t.integer  "penalty_amount_in_cents"
    t.string   "currency"
    t.boolean  "emails_sent",                :default => false
    t.datetime "created_at",                                    :null => false
    t.datetime "updated_at",                                    :null => false
    t.integer  "number"
    t.integer  "round"
    t.boolean  "last",                       :default => false
    t.boolean  "paused",                     :default => false
    t.integer  "member_count",               :default => 0
    t.integer  "recipient_id"
  end

  add_index "cycles", ["group_id"], :name => "index_cycles_on_group_id"
  add_index "cycles", ["recipient_id"], :name => "index_cycles_on_recipient_id"

  create_table "delayed_jobs", :force => true do |t|
    t.integer  "priority",   :default => 0
    t.integer  "attempts",   :default => 0
    t.text     "handler"
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  add_index "delayed_jobs", ["priority", "run_at"], :name => "delayed_jobs_priority"

  create_table "email_signups", :force => true do |t|
    t.string   "email"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "groups", :force => true do |t|
    t.string   "name"
    t.integer  "cycle_amount_in_cents",           :default => 0,      :null => false
    t.date     "start_date"
    t.string   "payment_cycle_unit"
    t.integer  "payment_cycle_quantity",          :default => 1
    t.string   "penalty_cycle_unit",              :default => "days"
    t.integer  "penalty_cycle_quantity",          :default => 1
    t.datetime "created_at",                                          :null => false
    t.datetime "updated_at",                                          :null => false
    t.integer  "penalty_amount_in_cents",         :default => 0,      :null => false
    t.string   "currency"
    t.integer  "alert_mode",                      :default => 1
    t.boolean  "in_session",                      :default => false
    t.integer  "round",                           :default => 1
    t.string   "penalty_alert_timespan_unit",     :default => "days"
    t.integer  "penalty_alert_timespan_quantity", :default => 1
    t.text     "description"
  end

  create_table "invites", :force => true do |t|
    t.integer  "group_id"
    t.string   "email"
    t.string   "first_name"
    t.string   "last_name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "member_id"
    t.string   "join_link"
  end

  add_index "invites", ["group_id"], :name => "index_invites_on_group_id"
  add_index "invites", ["member_id"], :name => "index_invites_on_member_id"

  create_table "member_specs", :force => true do |t|
    t.integer  "member_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "member_specs", ["member_id"], :name => "index_member_specs_on_member_id"

  create_table "members", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.string   "msisdn"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "username"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
  end

  add_index "members", ["email"], :name => "index_members_on_email", :unique => true
  add_index "members", ["reset_password_token"], :name => "index_members_on_reset_password_token", :unique => true

  create_table "memberships", :force => true do |t|
    t.integer  "group_id"
    t.integer  "member_id"
    t.datetime "created_at",                        :null => false
    t.datetime "updated_at",                        :null => false
    t.boolean  "admin",          :default => false
    t.integer  "cycle_position"
  end

  add_index "memberships", ["group_id"], :name => "index_memberships_on_group_id"
  add_index "memberships", ["member_id"], :name => "index_memberships_on_member_id"

  create_table "payments", :force => true do |t|
    t.string   "type"
    t.integer  "member_id"
    t.integer  "cycle_amount_in_cents",   :default => 0,     :null => false
    t.string   "currency"
    t.string   "string"
    t.boolean  "penalty_included",        :default => false
    t.datetime "paid_at"
    t.datetime "created_at",                                 :null => false
    t.datetime "updated_at",                                 :null => false
    t.integer  "cycle_id"
    t.integer  "recipient_id"
    t.integer  "penalty_amount_in_cents", :default => 0,     :null => false
  end

  add_index "payments", ["cycle_id"], :name => "index_payments_on_cycle_id"
  add_index "payments", ["member_id"], :name => "index_payments_on_member_id"
  add_index "payments", ["recipient_id"], :name => "index_payments_on_recipient_id"

end
