class CreateInvites < ActiveRecord::Migration
  def change
    create_table :invites do |t|
      t.references :group
      t.string :email
      t.string :first_name
      t.string :last_name

      t.timestamps
    end
    add_index :invites, :group_id
  end
end
