class SetDefaultForPenaltyAlertTimespanUnitInGroups < ActiveRecord::Migration
  def up
    change_column :groups, :penalty_alert_timespan_unit, :string, :default => 'days'
  end

  def down
    change_column :groups, :penalty_alert_timespan_unit, :string
  end
end
