class RenameSessionToRoundInCycles < ActiveRecord::Migration
  def up
    rename_column :cycles, :session, :round
  end

  def down
    rename_column :cycles, :round , :session
  end
end
