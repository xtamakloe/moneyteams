class AddAlertsModeToGroups < ActiveRecord::Migration
  def change
    add_column :groups, :alert_mode, :integer, :default => 1
  end
end
