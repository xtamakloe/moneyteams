class AddRecipientIdToCycles < ActiveRecord::Migration
  def change
    add_column :cycles, :recipient_id, :integer
    add_index :cycles, :recipient_id
  end
end
