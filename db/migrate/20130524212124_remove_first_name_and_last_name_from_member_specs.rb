class RemoveFirstNameAndLastNameFromMemberSpecs < ActiveRecord::Migration
  def up
    remove_column :member_specs, :first_name
    remove_column :member_specs, :last_name
  end

  def down
    add_column :member_specs, :last_name, :string
    add_column :member_specs, :first_name, :string
  end
end
