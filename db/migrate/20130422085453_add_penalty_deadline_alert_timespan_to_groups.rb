class AddPenaltyDeadlineAlertTimespanToGroups < ActiveRecord::Migration
  def change
    add_column :groups, :penalty_alert_timespan_unit, :string
    add_column :groups, :penalty_alert_timespan_quantity, :integer, :default => 1
  end
end
