class CreateCycles < ActiveRecord::Migration
  def change
    create_table :cycles do |t|
      t.references :group
      t.boolean :current, :default => false
      t.boolean :cycle_complete, :default => false
      t.integer :pay_in_amount_in_cents
      t.integer :pay_out_amount_in_cents
      t.integer :penalty_amount_in_cents
      t.string :currency
      t.boolean :emails_sent, :default => false

      t.timestamps
    end
    add_index :cycles, :group_id
  end
end
