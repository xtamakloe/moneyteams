class AddPenaltyAmountToPayments < ActiveRecord::Migration
  def change
    add_column :payments, :penalty_amount_in_cents, :integer, default: 0, null: false
  end
end
