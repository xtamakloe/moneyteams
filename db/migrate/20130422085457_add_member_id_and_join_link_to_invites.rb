class AddMemberIdAndJoinLinkToInvites < ActiveRecord::Migration
  def change
    add_column :invites, :member_id, :integer
    add_index :invites, :member_id

    add_column :invites, :join_link, :string
  end
end
