class AddPenaltyAmountInCentsToGroups < ActiveRecord::Migration
  def change
    add_column :groups, :penalty_amount_in_cents, :integer, :default => 0, :null => false
    add_column :groups, :currency, :string
  end
end
