class AddMemberCountToCycles < ActiveRecord::Migration
  def change
    add_column :cycles, :member_count, :integer, default: 0
  end
end
