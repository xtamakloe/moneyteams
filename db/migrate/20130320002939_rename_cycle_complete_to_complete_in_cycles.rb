class RenameCycleCompleteToCompleteInCycles < ActiveRecord::Migration
  def up
    rename_column :cycles, :cycle_complete, :complete
  end

  def down
    rename_column :cycles, :complete, :cycle_complete
  end
end
