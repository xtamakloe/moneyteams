class RenameSessionToRoundInGroups < ActiveRecord::Migration
  def up
    rename_column :groups, :session, :round
  end

  def down
    rename_column :groups, :round, :session
  end
end
