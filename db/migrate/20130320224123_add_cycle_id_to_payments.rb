class AddCycleIdToPayments < ActiveRecord::Migration
  def change
    add_column :payments, :cycle_id, :integer
    add_index :payments, :cycle_id
  end
end
