class CreateGroups < ActiveRecord::Migration
  def change
    create_table :groups do |t|
      t.string :name
      t.integer :cycle_amount_in_cents, :default => 0, :null => false
      t.date :start_date
      t.string :payment_cycle_unit
      t.integer :payment_cycle_quantity, :default => 1
      t.string :penalty_cycle_unit
      t.integer :penalty_cycle_quantity, :default => 1

      t.timestamps
    end
  end
end
