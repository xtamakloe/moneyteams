class AddRecipientIdToPayments < ActiveRecord::Migration
  def change
    add_column :payments, :recipient_id, :integer
    add_index :payments, :recipient_id
  end
end
