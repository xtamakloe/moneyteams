class RenameAmountInCentsToCycleAmountInCentsInGroups < ActiveRecord::Migration
  def up
    rename_column :groups, :amount_in_cents, :cycle_amount_in_cents
  end

  def down
    rename_column :groups, :cycle_amount_in_cents, :amount_in_cents
  end
end
