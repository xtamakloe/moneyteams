class AddInSessionToGroups < ActiveRecord::Migration
  def change
    add_column :groups, :in_session, :boolean, :default => false
  end
end
