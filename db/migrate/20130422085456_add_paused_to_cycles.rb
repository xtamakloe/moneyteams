class AddPausedToCycles < ActiveRecord::Migration
  def change
    add_column :cycles, :paused, :boolean, :default => false
  end
end
