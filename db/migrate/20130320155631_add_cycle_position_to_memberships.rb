class AddCyclePositionToMemberships < ActiveRecord::Migration
  def change
    add_column :memberships, :cycle_position, :integer
  end
end
