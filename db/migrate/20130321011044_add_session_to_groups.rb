class AddSessionToGroups < ActiveRecord::Migration
  def change
    add_column :groups, :session, :integer, :default => 1
  end
end
