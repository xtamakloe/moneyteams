class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.string :type
      t.references :member
      t.references :cycle_report
      t.integer :amount_in_cents, :default => 0, :null => false
      t.string :currency, :string
      t.boolean :penalty_included, :default => false
      t.datetime :paid_at

      t.timestamps
    end
    add_index :payments, :member_id
    add_index :payments, :cycle_report_id
  end
end
