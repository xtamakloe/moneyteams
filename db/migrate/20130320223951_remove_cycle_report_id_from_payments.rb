class RemoveCycleReportIdFromPayments < ActiveRecord::Migration
  def up
    remove_index :payments, :cycle_report_id
    remove_column :payments, :cycle_report_id
  end

  def down
    add_column :payments, :cycle_report_id, :integer
    add_index :payments, :cycle_report_id
  end
end
