class AddSessionToCycles < ActiveRecord::Migration
  def change
    add_column :cycles, :session, :integer
  end
end
