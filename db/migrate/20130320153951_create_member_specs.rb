class CreateMemberSpecs < ActiveRecord::Migration
  def change
    create_table :member_specs do |t|
      t.references :member
      t.string :first_name
      t.string :last_name

      t.timestamps
    end
    add_index :member_specs, :member_id
  end
end
