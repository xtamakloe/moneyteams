class AddLastToCycles < ActiveRecord::Migration
  def change
    add_column :cycles, :last, :boolean, :default => false
  end
end
