class RenamePayInAmountInCentsAndPayOutAmountInCentsInCycles < ActiveRecord::Migration
  def up
    rename_column :cycles, :pay_in_amount_in_cents, :recipients_amount_in_cents
    rename_column :cycles, :pay_out_amount_in_cents, :payers_amount_in_cents
  end

  def down
    rename_column :cycles, :payers_amount_in_cents, :pay_out_amount_in_cents
    rename_column :cycles, :recipients_amount_in_cents, :pay_in_amount_in_cents
  end
end
