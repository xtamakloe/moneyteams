class RenameAmountInPayments < ActiveRecord::Migration
  def up
    rename_column :payments, :amount_in_cents, :cycle_amount_in_cents
  end

  def down
    rename_column :payments, :cycle_amount_in_cents, :amount_in_cents
  end
end
