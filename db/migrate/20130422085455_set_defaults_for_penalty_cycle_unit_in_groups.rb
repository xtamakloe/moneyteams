class SetDefaultsForPenaltyCycleUnitInGroups < ActiveRecord::Migration
  def up
    change_column :groups, :penalty_cycle_unit, :string, :default => 'days'
  end

  def down
    change_column :groups, :penalty_cycle_unit, :string
  end
end
