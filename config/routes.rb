MoneyTeam::Application.routes.draw do

  devise_for :members, controllers: { registration: "auth/members/registrations",
                                      sessions:     "auth/members/sessions" }
  devise_scope :member do
    get  "/welcome"   => "auth/members/registrations#new"

    get  "/register"  => "auth/members/registrations#new"
    post "/register"  => "auth/members/registrations#create", as: :member_registration
    put  "/member/account/update"=> "auth/members/registrations#update"
    get  "/login"     => 'auth/members/sessions#new',    as: :new_member_session
    post "/login"     => 'auth/members/sessions#create', as: :member_session
  end

  # application
  scope module: :app do
    # user hub
    namespace :hub do
      resource  :account, path_names: { edit: :settings }
      resources :groups,        only: [ :index, :new, :create ],  path: "teams"
      resources :cycles,        only: :index
      resources :notifications, only: :index
      resources :messages,      except: [ :edit, :update ]
      resources :payments,      only: :index, path: "money"

      root to: "dashboard#show"
    end

    # teams
    scope module: :groups do
      resources :groups, path: "teams", path_names: { edit: :settings } do
        member do
          get  "members/join" => "groups#add_member",     as: :add_member
          put  "leave"        => "groups#remove_member",  as: :remove_member
          get  "rounds/start" => "groups#start_round",    as: :start_round
          get  "rounds/pause" => "groups#pause_round",    as: :pause_round
          get  "rounds/stop"  => "groups#stop_round",     as: :stop_round
          get  "current"      => "groups#current_cycle",  as: :current_cycle
        end

        resources :memberships, only: :update_multiple do
          put "update_multiple", on: :collection
        end
        resources :cycles, only: [ :show, :index ] do
          member do
            put "take_payment" => "cycles#acknowledge_payment", as: :acknowledge_payment
          end
        end
        resources :invites, only: [ :create, :destroy ]
        resources :members, only: [ :show, :index ]
        resources :reports, only: [ :show ]
      end
    end
  end


  # admin console
  namespace :admin do
  end


  # website
  scope module: :site do
    resources :groups, as: :site_groups, path: "welcome/teams", only: :create
    # do
    #   get "join", on: :member
    # end
    get   "homepage" => "pages#home"
    match "/"        => "pages#coming_soon"
  end

  root to: "site/pages#coming_soon"
end

=begin
  resources :groups, :path => "teams", path_names: { edit: :settings } do
    member do
      get  "join"
      get  "members/join" => "groups#add_member",     as: :add_member
      put  "leave"        => "groups#remove_member",  as: :remove_member
      post "rounds/start" => "groups#start_round",    as: :start_round
      get  "current"      => "groups#current_cycle",  as: :current_cycle
    end
    resources :cycles, only: [ :show, :index ] do
      put "payment_made" => "cycles#payment_made", as: :payment_made
    end
    resources :invites, only: [ :new, :create ]
    resources :members, only: [ :show, :index ]
    resources :memberships, only: :update_multiple do
      put "update_multiple", on: :collection
    end
  end
=end

=begin
  namespace :accounts, :path => "/hub" do
    resources :groups,        :only   => [ :index, :new, :create ],  :path => "teams"
    resources :cycles,        :only   => :index
    resources :notifications, :only   => :index
    resources :messages,      :except => [ :edit, :update ]

    root :to => "accounts#show", :as => :hub
  end
=end

# get     '/login'        => 'auth/bank_admins/sessions#new',         :as => :new_bank_admin_session
# post    '/login'        => 'auth/bank_admins/sessions#create',      :as => :bank_admin_session
# delete  '/logout'       => 'auth/bank_admins/sessions#destroy',     :as => :destroy_bank_admin_session
# get     '/account'      => 'auth/bank_admins/registrations#show',   :as => :bank_admin_registration
# get     '/account/settings' => 'auth/bank_admins/registrations#edit',   :as => :edit_bank_admin_registration
# put     '/account'      => 'auth/bank_admins/registrations#update'
# post    '/account'      => 'auth/bank_admins/registrations#create', :as => :bank_admin_registration
